package com.rostyslavprotsiv.model.action;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class SerializatorTest {
    private static Serializator serializator;

    @BeforeAll
    static void setUp() {
        serializator = new Serializator();
    }

    @ParameterizedTest
    @MethodSource("path")
    void testSerialize(String filePath) {
        assertTrue(serializator.serialize(new String("aaa"), filePath));
    }

    private static Stream<String> path() {
        return Stream.of("textserrr.txt");
    }
}