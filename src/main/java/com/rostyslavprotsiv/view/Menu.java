package com.rostyslavprotsiv.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Menu {
    private static final Logger LOGGER = LogManager.getLogger(Menu.class);

    public void welcome() {
        LOGGER.info("Welcome to my program!!");
    }

    public void outSerialization(Object obj) {
        LOGGER.info(obj.toString() + " - Object will be serialized.");
    }

    public void outSerializationComplete(Object obj) {
        LOGGER.info(obj.toString()
                + " - Object was serialized successfully!!");
    }

    public void outSerializationFailed(Object obj) {
        LOGGER.info(obj.toString()
                + " - Serialization failed!!");
    }

    public void outDeserialization() {
        LOGGER.info(" - Object will be deserialized.");
    }

    public void outDeserializationComplete(Object obj) {
        LOGGER.info(obj.toString()
                + " - Object was deserialized successfully!!");
    }

    public void outDeserializationFailed() {
        LOGGER.info("Deserialization failed!!");
    }

    public void outComputeTimeWithoutBufferedReader(long time) {
        LOGGER.info("Time of reading 200 MB file without BufferedReader : "
                + time);
    }

    public void outComputeTimeWithBufferedReader(long time) {
        LOGGER.info("Time of reading 200 MB file with BufferedReader : "
                + time);
    }

    public void outComputeTimeWithBufferedReader2KB(long time) {
        LOGGER.info("Time of reading 200 MB file with 2 KB BufferedReader : "
                + time);
    }

    public void outComputeTimeWithBufferedReader4KB(long time) {
        LOGGER.info("Time of reading 200 MB file with 4 KB BufferedReader : "
                + time);
    }

    public void outComputeTimeWithBufferedReader8KB(long time) {
        LOGGER.info("Time of reading 200 MB file with 8 KB BufferedReader : "
                + time);
    }

    public void outComputeTimeWithoutBufferedWriter(long time) {
        LOGGER.info("Time of writing 200 MB of characters into file"
                + " without BufferedWriter : "
                + time);
    }

    public void outComputeTimeWithBufferedWriter(long time) {
        LOGGER.info("Time of writing 200 MB of characters into file"
                + " with BufferedWriter : "
                + time);
    }

    public void outMyPushbackInputStreamRead(String str) {
        LOGGER.info("Read symbols: " + str);
    }

    public void outMyPushbackInputStreamUnread(String str) {
        LOGGER.info("Unread symbols: " + str);
    }

    public void outComments(String comments) {
        LOGGER.info("Comments from tokenizerAction:" + comments);
    }

    public void outFileStructure(String structure) {
        LOGGER.info("File structure: " + structure);
    }
}
