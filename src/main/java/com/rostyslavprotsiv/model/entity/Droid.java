package com.rostyslavprotsiv.model.entity;

import java.io.Serializable;
import java.util.Objects;

public class Droid implements Serializable {
    private int id;
    private String name;
    private transient double weight;
    private static final long serialVersionUID = 1L;

    public Droid() {
    }

    public Droid(int id, String name, double weight) {
        this.id = id;
        this.name = name;
        this.weight = weight;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Droid droid = (Droid) o;
        return id == droid.id && Double.compare(droid.weight, weight) == 0 && Objects.equals(name, droid.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, weight);
    }

    @Override
    public String toString() {
        return "Droid{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", weight=" + weight +
                '}';
    }
}
