package com.rostyslavprotsiv.model.entity;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;

public class Ship implements Serializable {
    private String name;
    private Droid[] droids;
    private transient String color;
    private static final long serialVersionUID = 1L;
//    private static final long serialVersionUID = 12L;

    public Ship() {
    }

    public Ship(String name, Droid[] droids, String color) {
        this.name = name;
        this.droids = droids;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Droid[] getDroids() {
        return droids;
    }

    public void setDroids(Droid[] droids) {
        this.droids = droids;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ship ship = (Ship) o;
        return Objects.equals(name, ship.name) && Arrays.equals(droids, ship.droids) && Objects.equals(color, ship.color);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, color);
        result = 31 * result + Arrays.hashCode(droids);
        return result;
    }

    @Override
    public String toString() {
        return "Ship{" +
                "name='" + name + '\'' +
                ", droids=" + Arrays.toString(droids) +
                ", color='" + color + '\'' +
                '}';
    }
}
