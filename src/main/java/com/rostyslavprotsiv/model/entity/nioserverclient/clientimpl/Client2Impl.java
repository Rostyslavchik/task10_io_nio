package com.rostyslavprotsiv.model.entity.nioserverclient.clientimpl;

import com.rostyslavprotsiv.model.entity.nioserverclient.client.Client;

public class Client2Impl {
    public static void main(String[] args) {
        Client client = new Client();
        client.start(new String[]{"The best way to predict the future is to create it.",
                "As you think, so shall you become.",
                "The noblest pleasure is the joy of understanding.",
                "Courage is grace under pressure.",
                "*exit*"});
    }
}
