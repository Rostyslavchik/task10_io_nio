package com.rostyslavprotsiv.model.entity.nioserverclient.client;

import com.rostyslavprotsiv.model.entity.nioserverclient.server.Server;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class Client {
    private static final int BUFFER_SIZE = 1024;
    private static final Logger LOGGER = LogManager.getLogger(Client.class);

    public void start(String[] messages) {
        LOGGER.info("Starting Client...");
        try {
            int port = 9999;
            InetAddress hostIP = InetAddress.getLocalHost();
            InetSocketAddress myAddress =
            new InetSocketAddress(hostIP, port);
            SocketChannel myClient = SocketChannel.open(myAddress);
            LOGGER.info(String.format("Trying to connect to %s:%d...",
                    myAddress.getHostName(), myAddress.getPort()));
            for (String msg: messages) {
                ByteBuffer myBuffer = ByteBuffer.allocate(BUFFER_SIZE);
                myBuffer.put(msg.getBytes());
                myBuffer.flip();
                int bytesWritten = myClient.write(myBuffer);
                LOGGER.info(String.format("Sending Message...: %s"
                                + "\nbytesWritten...: %d",
                                        msg, bytesWritten));
            }
            LOGGER.info("Closing Client connection...");
            myClient.close();
        } catch (IOException e) {
            LOGGER.info(e.getMessage());
            e.printStackTrace();
        }
    }
}
