package com.rostyslavprotsiv.model.entity.nioserverclient.serverimpl;

import com.rostyslavprotsiv.model.entity.nioserverclient.server.Server;

public class ServerImpl {
    public static void main(String[] args) {
        Server server = new Server();
        server.start();
    }
}
