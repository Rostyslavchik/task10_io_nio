package com.rostyslavprotsiv.model.action;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

public class FileAction {
    //I will use NIO2 package
    public String getAllFiles(String filePath) throws IOException {
        File f = new File(".");
        StringBuilder files = new StringBuilder();
        System.setProperty("user.dir", filePath); // set the current directory
        Path dir = Paths.get(f.getAbsolutePath());
        Files.walk(dir).forEach(i -> files.append(setSpaces(i.getNameCount())
                + i.getFileName() + ", Last modified: "
                + new Date(i.toFile().lastModified()) + ", Size: "
                + i.toFile().length() + "\n"));
        return files.toString();
    }

    private String setSpaces(int n) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < n; i ++) {
            builder.append(" ");
        }
        return builder.toString();
    }
}
