package com.rostyslavprotsiv.model.action;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class Serializator {
    private static final Logger LOGGER = LogManager.getLogger(Serializator.class);
    // some comments

    /**
     *
     * @param obj
     * @param filePath
     * @param <T>
     * @return
     */
    /*
    Some comments
     */
    //Comments!!
    public <T> boolean serialize(T obj, String filePath) {
        boolean flag = false;
        File file = new File(filePath);
        ObjectOutputStream objectOutputStream = null;
        try {
            FileOutputStream fos = new FileOutputStream(file);
            if (fos != null) {
                objectOutputStream = new ObjectOutputStream(fos);
                objectOutputStream.writeObject(obj);
                flag = true;
            }
        } catch (FileNotFoundException e) {
            LOGGER.error(e.getMessage() + "/" + e.getCause());
        } catch (NotSerializableException e) {
            LOGGER.fatal(e.getMessage() + "/" + e.getCause());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (objectOutputStream != null) {
                    objectOutputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return flag;
    }

    public <T> T deserialize(String filePath) throws InvalidObjectException {
        File file = new File(filePath);
        ObjectInputStream objectInputStream = null;
        try {
            FileInputStream fis = new FileInputStream(file);
            if (fis != null) {
                objectInputStream = new ObjectInputStream(fis);
                T obj = (T) objectInputStream.readObject();
                return obj;
            }
        } catch (FileNotFoundException e) {
            LOGGER.error(e.getMessage() + " " + e.getCause());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if (objectInputStream != null) {
                    objectInputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        throw new InvalidObjectException("Object was not recovered!");
    }
}
