package com.rostyslavprotsiv.model.action;


import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StreamTokenizer;
import java.util.LinkedList;
import java.util.List;

public class TokenizerAction {
    private static StreamTokenizer tokenizer;

    public String getComments(String filepath) throws IOException {
        tokenizer = new StreamTokenizer(new FileReader(filepath));
        StringBuilder builder = new StringBuilder();
        tokenizer.eolIsSignificant(true);
        tokenizer.ordinaryChar('/');
        while (tokenizer.nextToken() != StreamTokenizer.TT_EOF) {
            if (tokenizer.ttype == '/') {
                builder.append((char) tokenizer.ttype);
                builder.append(getComment(tokenizer));
            }
        }
        return builder.toString();
    }

    private String getComment(StreamTokenizer tokenizer) throws IOException {
        StringBuilder builder = new StringBuilder();
        int token = tokenizer.nextToken();
        if (tokenizer.ttype == '/') {
            builder.append((char) tokenizer.ttype);
            token = tokenizer.nextToken();
            while (tokenizer.ttype != StreamTokenizer.TT_EOL) {
                addValue(builder, tokenizer, token);
                token = tokenizer.nextToken();
                if (tokenizer.ttype == StreamTokenizer.TT_EOL) {
                    builder.append("\n");
                }
            }
        } else if (tokenizer.ttype == '*') {
            builder.append((char) tokenizer.ttype);
            token = tokenizer.nextToken();
            while (token != StreamTokenizer.TT_EOF && tokenizer.ttype != '/') {
                addValue(builder, tokenizer, token);
                token = tokenizer.nextToken();
                if (tokenizer.ttype == '/') {
                    builder.append((char) token + "\n");
                }
            }
        }
        return builder.toString();
    }

    private void addValue(StringBuilder builder, StreamTokenizer tokenizer,
                          int token) {
        if (tokenizer.ttype == StreamTokenizer.TT_WORD) {
            builder.append(tokenizer.sval + " ");
        } else if (tokenizer.ttype == '\'' || tokenizer.ttype == '"') {
            builder.append((char) tokenizer.ttype
                    + tokenizer.sval + (char) tokenizer.ttype);
        } else if (tokenizer.ttype == StreamTokenizer.TT_NUMBER) {
            builder.append(tokenizer.nval + " ");
        } else if (tokenizer.ttype == StreamTokenizer.TT_EOL) {
            builder.append("\n");
        } else {
            builder.append((char) token + " ");
        }
    }
}
