package com.rostyslavprotsiv.model.action;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

public class BufferedPerformanceTester {
    private static final int KB = 1024;
    private static final String FILE_PATH
            = ".\\src\\main\\resources\\someText.txt";
    private static final String FILE_PATH1
            = ".\\src\\main\\resources\\someText1.txt";
    private static List<Character> charsForTest;

    static {
        charsForTest = readFile();
    }

    public long computeTimeWithoutBufferedReader() {
        File f = new File(FILE_PATH);
        int data;
        long timeBefore = System.nanoTime();
        long timeAfter = 0;
        try (Reader reader = new FileReader(f)) {
            data = reader.read();
            System.out.println(data);
            while (data != -1) {
                data = reader.read();
            }
            timeAfter = System.nanoTime();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return timeAfter - timeBefore;
    }

    public long computeTimeWithBufferedReader() {
        return computeTimeWithBufferedReader(-1);
    }

    private long computeTimeWithBufferedReader(int bufferSize) {
        File f = new File(FILE_PATH);
        int data;
        long timeBefore = System.nanoTime();
        long timeAfter = 0;
        try (Reader reader = (bufferSize == -1 ?
                new BufferedReader(new FileReader(f)) :
                new BufferedReader(new FileReader(f), bufferSize))) {
            data = reader.read();
            System.out.println(data);
            while (data != -1) {
                data = reader.read();
            }
            timeAfter = System.nanoTime();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return timeAfter - timeBefore;
    }

    public long computeTimeWithBufferedReader2Kb() {
        return computeTimeWithBufferedReader(2 * KB);
    }

    public long computeTimeWithBufferedReader4Kb() {
        return computeTimeWithBufferedReader(4 * KB);
    }

    public long computeTimeWithBufferedReader8Kb() {
        return computeTimeWithBufferedReader(8 * KB);
    }

    public long computeTimeWithoutBufferedWriter() {
        File f = new File(FILE_PATH1);
        long timeBefore = System.nanoTime();
        long timeAfter = 0;
        try (Writer writer = new FileWriter(f)) {
            for (char ch: charsForTest) {
                writer.write(ch);
            }
            timeAfter = System.nanoTime();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return timeAfter - timeBefore;
    }

    public long computeTimeWithBufferedWriter() {
        File f = new File(FILE_PATH1);
        long timeBefore = System.nanoTime();
        long timeAfter = 0;
        try (Writer writer = new BufferedWriter(new FileWriter(f))) {
            for (char ch: charsForTest) {
                writer.write(ch);
            }
            timeAfter = System.nanoTime();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return timeAfter - timeBefore;
    }

    private static List<Character> readFile() {
        File f = new File(FILE_PATH);
        List<Character> chars = new LinkedList<>();
        int data;
        try (Reader reader = new BufferedReader(new FileReader(f))) {
            data = reader.read();
            while (data != -1) {
                chars.add((char) data);
                data = reader.read();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return chars;
    }
}
