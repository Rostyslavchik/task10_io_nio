package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.action.BufferedPerformanceTester;
import com.rostyslavprotsiv.model.action.FileAction;
import com.rostyslavprotsiv.model.action.Serializator;
import com.rostyslavprotsiv.model.action.TokenizerAction;
import com.rostyslavprotsiv.model.entity.Droid;
import com.rostyslavprotsiv.model.entity.MyPushbackInputStream;
import com.rostyslavprotsiv.model.entity.Ship;
import com.rostyslavprotsiv.view.Menu;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InvalidObjectException;

public class Controller {
    private static final Serializator SERIALIZATOR = new Serializator();
    private static final Menu MENU = new Menu();
//    private static final BufferedPerformanceTester TESTER
//            = new BufferedPerformanceTester();
    private static String filePath = "filee.txt";
    private static final String SRC_PATH = ".\\src\\main\\java\\com"
            + "\\rostyslavprotsiv\\model\\action\\Serializator.java";
    private static final String TEST_PATH = "C:\\Users\\User\\IdeaProjects"
            + "\\task10_io_nio\\src";
    private static final String TEST_PATH1 = "C:\\Users\\User\\IdeaProjects"
            + "\\task10_io_nio\\src\\main\\java";
    private static final TokenizerAction TOKENIZER_ACTION = new TokenizerAction();
    private static final FileAction FILE_ACTION = new FileAction();

    public void execute() {
        long timeWithoutBufferedReader;
        long timeWithBufferedReader;
        long timeWithBufferedReader2KB;
        long timeWithBufferedReader4KB;
        long timeWithBufferedReader8KB;
        long timeWithoutBufferedWriter;
        long timeWithBufferedWriter;
        boolean serialized;
        Droid droid1 = new Droid(1, "aaa", 1.1);
        Droid droid2 = new Droid(2, "bbb", 2.2);
        Droid droid3 = new Droid(3, "ccc", 3.3);
        Droid[] droids = new Droid[] {droid1, droid2, droid3};
        Ship ship = new Ship("sss", droids, "black");
        Ship deserializedShip = null;
        MENU.welcome();
//        MENU.outSerialization(ship);
//        serialized = SERIALIZATOR.serialize(ship, filePath);
//        if (serialized) {
//            MENU.outSerializationComplete(ship);
//        } else {
//            MENU.outSerializationFailed(ship);
//        }
        MENU.outDeserialization();
        try {
            deserializedShip = SERIALIZATOR.deserialize(filePath);
        } catch (InvalidObjectException e) {
            e.printStackTrace();
        }
        if (deserializedShip != null) {
            MENU.outDeserializationComplete(deserializedShip);
        } else {
            MENU.outDeserializationFailed();
        }
//        timeWithoutBufferedReader = TESTER.computeTimeWithoutBufferedReader();
//        MENU.outComputeTimeWithoutBufferedReader(timeWithoutBufferedReader);
//        timeWithBufferedReader = TESTER.computeTimeWithBufferedReader();
//        MENU.outComputeTimeWithBufferedReader(timeWithBufferedReader);
//        timeWithBufferedReader2KB = TESTER.computeTimeWithBufferedReader2Kb();
//        MENU.outComputeTimeWithBufferedReader2KB(timeWithBufferedReader2KB);
//        timeWithBufferedReader4KB = TESTER.computeTimeWithBufferedReader4Kb();
//        MENU.outComputeTimeWithBufferedReader4KB(timeWithBufferedReader4KB);
//        timeWithBufferedReader8KB = TESTER.computeTimeWithBufferedReader8Kb();
//        MENU.outComputeTimeWithBufferedReader8KB(timeWithBufferedReader8KB);
//        timeWithoutBufferedWriter = TESTER.computeTimeWithoutBufferedWriter();
//        MENU.outComputeTimeWithoutBufferedWriter(timeWithoutBufferedWriter);
//        timeWithBufferedWriter = TESTER.computeTimeWithBufferedWriter();
//        MENU.outComputeTimeWithBufferedWriter(timeWithBufferedWriter);
        try {
            testMyPushbackInputStream();
            MENU.outComments(TOKENIZER_ACTION.getComments(SRC_PATH));
            MENU.outFileStructure(FILE_ACTION.getAllFiles(TEST_PATH));
            MENU.outFileStructure(FILE_ACTION.getAllFiles(TEST_PATH1));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void testMyPushbackInputStream() throws IOException {
        int pushbackLimit = 5;
        StringBuilder builder = new StringBuilder();
        MyPushbackInputStream stream = new MyPushbackInputStream(
                new FileInputStream(filePath), pushbackLimit);
        for (int i = 0; i < 30; i ++) {
            builder.append((char) stream.read());
        }
        MENU.outMyPushbackInputStreamRead(builder.toString());
        for (int i = 29; i >= 25; i --) {
            stream.unread(builder.charAt(i));
        }
        builder = new StringBuilder();
        for (int i = 29; i >= 25; i --) {
            builder.append((char) stream.read());
        }
        stream.close();
        MENU.outMyPushbackInputStreamUnread(builder.toString());
    }
}
